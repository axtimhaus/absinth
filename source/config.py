import os

'''
Configuration file
'''

'''
Choose paths for corpus, dataset and output.
- The output directory should be empty when starting absinth.
'''
graph_path = os.path.join(os.path.dirname(__file__), ".graphs/")
corpus = "/proj/absinth/wikipedia_shuffled2/"
trialset = os.path.join(os.path.dirname(__file__), "../evaluator/datasets/trial/")
testset = os.path.join(os.path.dirname(__file__), "../evaluator/datasets/test/")
devset = os.path.join(os.path.dirname(__file__), "../evaluator/datasets/development/")
output = os.path.join(os.path.dirname(__file__), "../output/")
final = os.path.join(os.path.dirname(__file__), "../output/.final/")

'''
Disambiguation Pipeline
There are multiple disambiguation methods implemented. Specify the order in
which they should be merged.
The first method with a positive result is used and methods labeled with 0 are ignored.
At least one method must be given a value != 0.
'''
propagation_rank = 1
mst_rank = 2

'''
Choose stop words and allowed pos-tags.
- Stop words will not be considered for nodes.
- Only tokens with allowed pos-tags will be considered.
'''
stop_words = ['utc', "'s", 'new', 'p.', 'first', 'other', 'talk', 'wikipedia', 'article', 'topic', 'page', 'editors', 'encyclopedia', 'free', 'pp', 'twitter', 'facebook', 'youtube', 'copyright', '®', '|']
allowed_tags = ['NN','NNS','JJ','JJS','JJR','NNP']

'''
Choose the maximum number of nodes and edges that should be considered before building the graph.
'''
max_nodes = 20000
max_edges = 2000000

'''
Choose the minimum and maximum context size.
'''
min_context_size = 4
max_context_size = 20

'''
Choose filters for building the graph.

- Use dynamic filters for nodes and edges (mean frequencies of occurences) (ABSINTH uses this as a standard) 
or
- Only consider occurrences/cooccurrences for nodes/edges, that occur more often than these values. (Hyperlex uses these (nodes: 10, edges: 5)))

- Only consider edges with a weight beneath the maximum weight
'''
use_mean_freq = True
min_node_freq = 10
min_edge_freq = 5

max_weight = 0.9

'''
Choose minimum number of neighbours and maximum median weight of the most frequent neighbours of a node for root hubs. 
- (the threshold is calculated using the median of the same number of neighbours declared in min_neighbours)
'''
min_neighbours = 5
threshold = 0.8

'''
Choose whether or not the tokens should be lemmatised.
'''
lemma = False

'''
Propagation options
'''
max_propagation_iteration_count = 40

'''
Baseline options
'''
sense_count = 10

