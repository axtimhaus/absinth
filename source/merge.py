#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Merges .absinth-files into one document.

Collect file names from the directory specified in config.py. Ignore every first
line and merge the rest. Write to file with timestamp as name.

"""

import config
import os
import sys
import time

def main():
    
    file_path = config.output

    files = [file_path+f for f in os.listdir(file_path) if '.absinth' in f]

    results = list()

    for f in files:
        with open(f, 'r') as lines:
            for line in lines.readlines()[1:]:
                results.append(line.split('\t'))
                
    results = sorted(results, key=lambda x: x[0])

    time_int = int(time.strftime("%Y%m%d%H%M%S")[2:])

    final_path = config.final+'{}.absinth'.format(hex(time_int)[2:])
    new_path = config.final+'.new.absinth'
    
    final_file = open(final_path, 'w')
    new_file = open(new_path, 'w')

    final_file.write('subTopicID\tresultID\n')
    new_file.write('subTopicID\tresultID\n')

    for r in results:
        final_file.write('\t'.join(r))
        new_file.write('\t'.join(r))
        
    final_file.close()
    new_file.close()
    

if __name__ == "__main__":
    main()
