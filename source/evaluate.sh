#!/bin/bash

THIS_DIR=$(dirname "${BASH_SOURCE[0]}")

cd "$THIS_DIR/../evaluator/"

java -jar "$THIS_DIR/../evaluator/WSI-Evaluator.jar" "$THIS_DIR/../evaluator/datasets/$1/" "$THIS_DIR/../output/.final/.new.absinth"
cp "$THIS_DIR/../evaluator/wsi_eval.log" "$THIS_DIR/../results/"
rm "$THIS_DIR/../output/"*.absinth


