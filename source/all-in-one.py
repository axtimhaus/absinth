#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""All-In-One Baseline for ABSINTH

This tool works as a All-In-One Clustering Baseline

.. _Association Based Semantic Induction Tools from Heidelberg
    https://gitlab.cl.uni-heidelberg.de/zimmermann/absinth

"""
import config
import time
import sys

time_int = int(time.strftime("%Y%m%d%H%M%S")[2:])
final_path = config.final+'{}.absinth'.format(hex(time_int)[2:])
new_path = final_path = config.final+'.new.absinth'

results = open(final_path, 'w')
new_results = open(new_path, 'w')

if '-test' in sys.argv:
    data_path = config.testset
elif '-dev' in sys.argv:
    data_path = config.devset
elif '-trial' in sys.argv:
    data_path = config.trialset
else:
    data_path = config.trialset

with open(data_path+'results.txt', 'r') as f:
    
    results.write('subTopicID\tresultID\n')
    new_results.write('subTopicID\tresultID\n')
    lines = f.readlines()
    
    for line in lines[1:]:
        
        result_id = line.split()[0]
        results.write(result_id.split('.')[0]+'.1\t'+result_id+'\n')
        new_results.write(result_id.split('.')[0]+'.1\t'+result_id+'\n')
        
results.close()
new_results.close()
