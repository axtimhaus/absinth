#!/bin/bash

THIS_DIR=$(dirname "${BASH_SOURCE[0]}")

mkdir "$THIS_DIR/../output/"
mkdir "$THIS_DIR/../output/.final/"

mkdir "$THIS_DIR/../results/"

python3 -m venv absinth_env
source absinth_env/bin/activate

pip3 install --upgrade pip
pip3 install networkx
pip3 install numpy
pip3 install scipy
pip3 install spacy
pip3 install matplotlib

python3 -m spacy download en
