# absinth

Absinth is a graph based approach to word sense induction. Out system is based on Hyperlex (Véronis 2004) and made for SemEval-2013 Task 11 (Navigli, Vannella).

## Getting Started

Alrighty then!

Start by cloning our system using git.

```
$ git clone https://gitlab.cl.uni-heidelberg.de/zimmermann/absinth/
```

### Prerequisites

Download the evaluator and dataset from the task page and place them in the appropriate folders. This repository already includes a version of this evaluator. For proper testing however, the official one should be used.

https://www.cs.york.ac.uk/semeval-2013/task11/index.php%3Fid=data.html

If you choose to use our installer (`setup.sh`), make sure Python 3.6 is installed, as it may not work with older versions.


### Installing

Change to source directory.

```
$ cd absinth/source/
```

Run `setup.sh`. This installs mandatory modules and creates folders for
temporary output.

```
$ bash setup.sh
```

Specify paths and (if appropriate) other variables for our system within `config.py`.

```
$ emacs config.py
```

## Deployment

Activate the virtual environment with your dependencies.

```
$ source absinth_env/bin/activate
```

Run `absinth.py` without modifiers to run on trial data.

```
$ python3 absinth.py
```

Specify on which data to run with the commands `-test`, `-trial` and `-dev`

```
$ python3 absinth.py -dev
```

Absinth may utilise multiprocessing for parallel topic processing. Simply put
'-p' followed by the number of processes after the program name.

```
$ python3 absinth.py -p 4
```

Absinth produces individual clustering files for every target. To merge the 
output files, simple call `merge.py`. The resulting output for the WSI-evaluator
is placed in the 'absinth/output/.final/' directory.

```
$ python3 merge.py
```

Absinth does not overwrite already processed topics. Please delete files you
wish to replace manually.

## Baseline

Our baseline `abstinent.py` works in much the same way as `absinth.py`. Not all
variables in `config.py` are supported in our baseline though.

```
$ python3 abstinent.py -test -p 3
```

To merge the `abstinent.py` output, simply run it the same way as before:

```
$ python3 merge.py
```

`all-in-one.py` and `singletons.py` do not need to be merged! Calling `merge.py` after running either
of these programs results in overwriting their output.

## Evaluation

We employ the same evaluator as the orginal SemEval-2013 Task, but simplified the process.
Just call `evaluate.sh` with the set you want to evaluate on to evaluate the most recent merge.

```
$ bash evaluate.sh development
```

`evaluate.sh` removes every absinth-file from the output directory, so make sure every target word has been merged before evaluating.

## Built With

* [NetworkX](https://networkx.github.io/) - Graph implementation
* [Spacy](https://spacy.io/) - Tokenisation and syntactic parsing
* [NumPy](http://www.numpy.org/) - Maths
* [SciPy](https://www.scipy.org/) - More maths

## Cite as
Victor Zimmermann and Maja Hoffmann. 2022. Absinth: A small world approach to word sense induction. In Proceedings of the 18th Conference on Natural Language Processing (KONVENS 2022), pages 121–128, Potsdam, Germany. KONVENS 2022 Organizers.

```
@inproceedings{zimmermann-hoffmann-2022-absinth,
    title = "{A}bsinth: A small world approach to word sense induction",
    author = "Zimmermann, Victor  and
      Hoffmann, Maja",
    booktitle = "Proceedings of the 18th Conference on Natural Language Processing (KONVENS 2022)",
    month = "12--15 " # sep,
    year = "2022",
    address = "Potsdam, Germany",
    publisher = "KONVENS 2022 Organizers",
    url = "https://aclanthology.org/2022.konvens-1.14",
    pages = "121--128",
}
```

## References

Di Marco, Antonio and Navigli, Roberto (2013). Clustering and Diversifying Web Search Results with Graph-Based Word Sense Induction. Computational Linguistics, 39(3). doi:10.1162/COLI_a_00148

Hamilton, William L. and Clark, Kevin and Leskovec, Jure and Jurafsky, Dan. Inducing Domain-Specific Sentiment Lexicons from Unlabeled Corpora. Proceedings of the 2016 Conference on Empirical Methods in Natural Language Processing, 595--605. doi:10.18653/v1/D16-1057

Kruskal, J. (1956). On the Shortest Spanning Subtree of a Graph and the Traveling Salesman Problem. Proceedings of the American Mathematical Society, 7(1), 48-50. doi:10.2307/2033241

Véronis, Jean. (2004). HyperLex: Lexical cartography for information retrieval. Computer Speech & Language. doi:10.1016/j.csl.2004.05.002

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* In Loving Memory of Bente Nittka.
